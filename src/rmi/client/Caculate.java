package rmi.client;

import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import rmi.common.Compute;
import rmi.entity.Strategy;

public class Caculate {
	
	public static void main(String[] args) {
		
		if(System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
			System.out.println("Security manager istalled...");
		}
	//-Djava.security.policy=client.policy
		
		String host = "localhost";
		int port = 1989;
		
		try {
			Registry registry = LocateRegistry.getRegistry(host, port);
			Compute m = (Compute) registry.lookup("math");

			System.out.println(m.add(1, 2));
			
			System.out.println("Strategy:===" + m.deploy(new Strategy("minhnd")));
			
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}
}
